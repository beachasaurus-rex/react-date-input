const path = require('path');
var PACKAGE = require('./package.json');
var version = PACKAGE.version;

module.exports = {
    resolve: {
        modules: [
            path.join(__dirname, 'src'),
            'node_modules'
        ],
    },
    output: {
        filename: `Dateinput-v${version}.bundle.js`
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                    },
                ],
            },
        ],
    }
};
