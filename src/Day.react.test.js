import React from 'react';
import {v4 as uuidv4} from 'uuid';
import renderer from 'react-test-renderer';

import { Day, DayData } from './Day.js';

describe('DayData', () => {
    test('is constructed correctly', () => {
        let id = uuidv4();
        let num = '2';
        let isSelected = false;
        let day = new DayData(id, num, isSelected);
        expect(day.id).toBe(id);
        expect(day.number).toBe(num);
        expect(day.isSelected).toBe(isSelected);
    });
});

describe('Day', () => {
    test('has correct style', () => {
        const expDay = renderer.create(
          <Day id={1} number={2} isSelected={false}/>
        );
        let tree = expDay.toJSON();
        expect(tree.props.className).toBe('day');
    });
    
    test('Selected day has correct style', () => {
      const expDay = renderer.create(
        <Day id={1} number={2} isSelected={true}/>
      );
      let tree = expDay.toJSON();
      expect(tree.props.className).toBe('daySelected');
    });
    
    test('Empty day has correct style', () => {
        const expDay = renderer.create(
          <Day id={1} number={''} isSelected={false}/>
        );
        let tree = expDay.toJSON();
        expect(tree.props.className).toBe('dayEmpty');
    });
    
    test('onClick callback is called', () => {
        let click = jest.fn();
        const expDay = renderer.create(
          <Day id={1} number={2} isSelected={false} handleClick={click}/>
        );
        let i = expDay.getInstance();
        i.onClick({'target': {'value': '2'}});
        expect(click).toHaveBeenCalled();
    });
});