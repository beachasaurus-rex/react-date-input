import React from 'react';
import {v4 as uuidv4} from 'uuid';

import { getMonthDays, getMonthName } from './date-data.js';
import styles from './Calendar.module.css';
import { DayData } from './Day.js';
import Month from './Month.js';
import WeekdayHeader from './WeekdayHeader.js';
import NavPanel from './NavPanel.js';

function buildDays(dayArr, startDay, startIndex, endIndex, selectedDay) {
    let dayIter = 0;
    for (let i = 0; i < dayArr.length; i++) {
        if (
            i >= startIndex
            && i < endIndex
        ) {
            dayArr[i] = new DayData(uuidv4(), startDay+dayIter, (startDay+dayIter) === selectedDay);
            dayIter++;
        } else {
            dayArr[i] = new DayData(uuidv4(), "", false);
        }
    }
}

function buildCalendar(monthIndex, year, selectedDay) {
    let calendar = new Array(6);
    let monthDays = getMonthDays(monthIndex, year);
    let firstWeekStartIndex = (new Date(year, monthIndex, 1)).getDay();
    let lastWeekEndIndex = monthDays - (4*7 + (7 - firstWeekStartIndex));
    let lastWeekIndex = -1;
    let secondLastWeekEndIndex;
    if (lastWeekEndIndex < 0) {
        lastWeekIndex = 4;
        secondLastWeekEndIndex = 7 + lastWeekEndIndex;
    }
    let dayIter = 1;
    for (let i = 0; i < calendar.length; i++) {
        calendar[i] = new Array(7);
        if (i === 0) {
            buildDays(calendar[i], dayIter, firstWeekStartIndex, 7, selectedDay);
            dayIter += 7-firstWeekStartIndex;
        } else if (i === lastWeekIndex) {
            buildDays(calendar[i], dayIter, 0, secondLastWeekEndIndex, selectedDay);
        } else if (i === calendar.length-1 && lastWeekIndex === -1) {
            buildDays(calendar[i], dayIter, 0, lastWeekEndIndex, selectedDay);
        } else if (i === calendar.length-1 && lastWeekIndex === 4) {
            buildDays(calendar[i], dayIter, 0, 0, selectedDay);
        } else {
            buildDays(calendar[i], dayIter, 0, 7, selectedDay);
            dayIter += 7;
        }
    }
    return calendar
}

export default class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.key = props.key;
        this.handleDayClick = this.handleDayClick.bind(this);
        this.onInputFocus = this.onInputFocus.bind(this);
        this.handlePrevMonthClick = this.handlePrevMonthClick.bind(this);
        this.handleNextMonthClick = this.handleNextMonthClick.bind(this);
        
        this.state = {
            calendarIsVisible: false,
            selectedDate: props.currentDate,
            cycleDate: props.currentDate,
            calendar: buildCalendar(props.currentDate.getMonth(), props.currentDate.getFullYear(), props.currentDate.getDate()),
        };
    }

    onInputFocus() {
        this.setState({
            calendarIsVisible: true,
            selectedDate: this.state.selectedDate,
            cycleDate: this.state.cycleDate,
            calendar: this.state.calendar,
        });
    }

    handleDayClick(dayNum) {
        if (dayNum > 0) {
            let newSelectedDate = new Date(this.state.cycleDate.getFullYear(), this.state.cycleDate.getMonth(), dayNum);
            let oldSelectedDay = this.state.cycleDate.getDate();
            let c = this.state.calendar;
            for (let i = 0; i < c.length; i++) {
                for (let j = 0; j < c[i].length; j++) {
                    if (c[i][j].number === oldSelectedDay) {
                        c[i][j].isSelected = false;
                    } else if (c[i][j].number === dayNum) {
                        c[i][j].isSelected = true;
                    }
                }
            }
            this.setState({
                calendarIsVisible: false,
                selectedDate: newSelectedDate,
                cycleDate: newSelectedDate,
                calendar: c,
            });
        }
    }

    handlePrevMonthClick() {
        let oldMonth = this.state.cycleDate.getMonth();
        let oldYear = this.state.cycleDate.getFullYear();
        let newMonth;
        let newYear;
        if (oldMonth === 0) {
            newMonth = 11;
            newYear = oldYear - 1;
        } else {
            newMonth = oldMonth - 1;
            newYear = oldYear;
        }
        let newDate = new Date(newYear, newMonth, 1);
        this.setState({
            calendarIsVisible: true,
            selectedDate: this.state.selectedDate,
            cycleDate: newDate,
            calendar: buildCalendar(newMonth, newYear, -1),
        });
    }

    handleNextMonthClick() {
        let oldMonth = this.state.cycleDate.getMonth();
        let oldYear = this.state.cycleDate.getFullYear();
        let newMonth;
        let newYear;
        if (oldMonth === 11) {
            newMonth = 0;
            newYear = oldYear + 1;
        } else {
            newMonth = oldMonth + 1;
            newYear = oldYear;
        }
        let newDate = new Date(newYear, newMonth, 1);
        this.setState({
            calendarIsVisible: true,
            selectedDate: this.state.selectedDate,
            cycleDate: newDate,
            calendar: buildCalendar(newMonth, newYear, -1),
        });
    }

    render() {
        let visibleStyle = "hidden";
        let inputField;
        if (this.state.calendarIsVisible) {
            visibleStyle = "visible";
            inputField = <input readOnly className={styles.dateInputSelected} type="text"
                    value={(this.state.selectedDate.toISOString()).substring(0,10)}
                    onFocus={this.onInputFocus}
                />;
        } else {
            inputField = <input readOnly className={styles.dateInput} type="text"
                    value={(this.state.selectedDate.toISOString()).substring(0,10)}
                    onFocus={this.onInputFocus}
                />;
        }

        return (
            <div key={uuidv4()}>
                <label className={styles.inputLabel}>
                    Select Date
                </label>
                {inputField}
                <div key={uuidv4()} className={styles.calendarContainer} style={{visibility: visibleStyle}}>
                    <div key={uuidv4()} className={styles.header}>
                        <NavPanel key={uuidv4()}
                            monthName={getMonthName(this.state.cycleDate.getMonth())}
                            year={this.state.cycleDate.getFullYear()}
                            handlePrevMonthClick={this.handlePrevMonthClick}
                            handleNextMonthClick={this.handleNextMonthClick}
                        />
                        <WeekdayHeader key={uuidv4()}/>
                    </div>
                    <Month key={uuidv4()} days={this.state.calendar} handleDayClick={this.handleDayClick}/>
                </div>
            </div>
        );
    }
}