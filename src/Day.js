import React from 'react';

import styles from './Day.module.css';

export class DayData {
	constructor(id, number, isSelected) {
		this.id = id;
		this.number = number;
		this.isSelected = isSelected;
	}
}

export class Day extends React.Component {
	constructor(props) {
		super(props);
		this.id = props.id;
		this.number = props.number;
		this.isSelected = props.isSelected;
		this.onClick = this.onClick.bind(this);
	}

	onClick(e) {
		let dayNum = parseInt(e.target.value, 10);
		this.props.handleClick(dayNum);
	}

	render() {
		let style;
		if (this.number !== "") {
			if (this.isSelected) {
				style = styles.daySelected;
			} else {
				style = styles.day;
			}
		} else {
			style = styles.dayEmpty;
		}

		return <input key={this.id}
			className={style}
			type="button"
			value={this.number}
			onClick={this.onClick}
		/>;
	}
}